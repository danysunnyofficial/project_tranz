Project to fix coordinates that are out of line
-----------------------------------------------
1.Demo-Preview
    1.1 Demo : img/screenshots/ss_plotted_figure.png
    1.2 "latitude_longitude_details.csv" is the list of latitude and longitudes
    1.3 "terrain_classification_test.csv" is the terrain list with kilometeres
    1.4 Project find the latitude and longitude coordinates that are out of line and
    automatically fix the same to form a continuous path.
    1.4.1 Generate DB of each latitude and longitude pair with matching terrain information
    1.4.2 List all the points with terrain “road” in it without “civil station”

2.Table of Contents
    2.1 Project Title
    2.2 Demo-Preview
    2.3 Table of Contents
    2.4 Installation
    
3. Installation
    Install the app requirements for development.
    Change permission to run.sh if needed.
    Execute ./run.sh
    
