FROM python:3.8-slim-buster

ENV DASH_DEBUG_MODE True
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8050
CMD ["python", "mlLatLongTerrain.py"]