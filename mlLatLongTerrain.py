import pandas as pd
import plotly.graph_objects as go
import dash

import dash
import dash_bootstrap_components as dbc
import flask


server = flask.Flask(__name__)
app = dash.Dash(__name__, server=server, external_stylesheets=[dbc.themes.BOOTSTRAP])
app.config.suppress_callback_exceptions = True
# Read from csv
df = pd.read_csv('latitude_longitude_details.csv')

# Convert Latitude and Logitude to seperate list
str_lat =  df["latitude"].tolist()
str_long = df["longitude"].tolist()

# Plot the figure
fig = go.Figure(data=go.Scattergeo(
    lat = str_lat,
    lon = str_long,
    mode = 'lines',
    line = dict(width = 2, color = 'blue'),
))
# Render 
app.layout = dash.html.Div(
    children=[        
        dash.html.Div(children="""Output"""),
        dash.dcc.Graph(
            id="graph", figure=fig
        ),
    ]
)

if __name__ == "__main__":
    import os

    debug = False if os.environ["DASH_DEBUG_MODE"] == "False" else True
    app.run_server(host="0.0.0.0", port=8050, debug=debug)
    
